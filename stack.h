#pragma once
#include <algorithm>
#include <iostream>

template<typename Type>
class Stack
{
public:
	Stack() = default;

	Stack(size_t sz)
		:ptr_(new Type[sz]) , capacity_(sz)
	{		
	}
	~Stack()
	{
		delete[] ptr_;
	}

	Type* Get()
	{
		return ptr_;
	}

	size_t GetSize()
	{
		return size_;
	}

	size_t GetCapacity()
	{
		return capacity_;
	}

	void Push(const Type& value)
	{
		if (size_ >= capacity_)
		{
			const size_t new_capacity = capacity_ == 0 ? 1 : capacity_ * 2;
			Stack tmp(new_capacity);
			std::copy(ptr_, ptr_ + size_, tmp.Get());
			std::swap(ptr_, tmp.ptr_);
			capacity_ = new_capacity;
		}
		ptr_[size_] = value;
		size_++;
	}

	void Show()
	{
		for (Type* p = ptr_; p < ptr_ + size_; ++p)
		{
			if (p)
				std::cout << *p << " ";
		}
		std::cout << std::endl;
	}

	void PopBack()
	{
		size_--;
	}

private:
	Type* ptr_ = nullptr;
	size_t capacity_ = 0;
	size_t size_ = 0;
};
﻿#include "stack.h"

#include <iostream>

int main()
{
	Stack<double> myStack;
	std::cout << myStack.GetSize()<< " " << myStack.GetCapacity() << std::endl;
	myStack.Push(10);
	myStack.Show();
	myStack.Push(1);
	myStack.Show();
	myStack.PopBack();
	myStack.Show();
	myStack.Push(10.1);
	myStack.Push(0);
	myStack.Show();
	myStack.PopBack();
	myStack.Show();
	std::cout << myStack.GetSize() << " " << myStack.GetCapacity() << std::endl;
}